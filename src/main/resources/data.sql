INSERT INTO ENDERECO (endereco_Id, rua, numero, bairro, cidade, estado, cep) VALUES(001, 'Rua XV Novembro', 20, 'Centro', 'Blumenau', 'SC', '89000-000');
INSERT INTO ENDERECO (endereco_Id, rua, numero, bairro, cidade, estado, cep) VALUES(002, 'Rua 07 de Setembro', 50, 'Centro', 'Blumenau', 'SC', '89000-000');
INSERT INTO ENDERECO (endereco_Id, rua, numero, bairro, cidade, estado, cep) VALUES(003, 'Rua Dr Blumenau', 1000, 'Centro', 'Indaial', 'SC', '83000-000');

INSERT INTO CONTA (num_Conta, saldo, limite, tipo, senha) VALUES(100, 200.00, 500.00, 1, 12345);
INSERT INTO CONTA (num_Conta, saldo, limite, tipo, senha) VALUES(200, 300.00, 250.00, 1, 67891);

INSERT INTO USUARIO (user_Id, senha, nome, cpf, tipo, end_endereco_id, num_Conta_id) VALUES('001', '1234', 'Janete', '000.000.000-00', 'programadora', 001, 100);
INSERT INTO USUARIO (user_Id, senha, nome, cpf, tipo, end_endereco_id, num_Conta_id) VALUES('002', '5678', 'João', '100.100.100-10', 'analista', 003, 200);
