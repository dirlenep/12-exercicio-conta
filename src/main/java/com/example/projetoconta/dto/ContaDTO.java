package com.example.projetoconta.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.example.projetoconta.model.Conta;

public class ContaDTO {
	private int numConta;
	private int tipo;
	
	public ContaDTO(Conta conta) {
		this.numConta = conta.getNumConta();
		this.tipo = conta.getTipo();
	}
	
	public int getNumConta() {
		return numConta;
	}
	public int getTipo() {
		return tipo;
	}

	public static List<ContaDTO> converter(List<Conta> contas){
		return contas.stream().map(ContaDTO::new).collect(Collectors.toList());
	}
}
