package com.example.projetoconta.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.example.projetoconta.model.Usuario;

public class UsuarioDTO {
	private String userId;
	private String nome;
	
	public UsuarioDTO(Usuario usuario) {
		this.userId = usuario.getUserId();
		this.nome = usuario.getNome();
	}
	
	public String getUserId() {
		return userId;
	}
	public String getNome() {
		return nome;
	}
	
	public static List<UsuarioDTO> converter(List<Usuario> usuarios){
		return usuarios.stream().map(UsuarioDTO::new).collect(Collectors.toList());
	}
	
	public static UsuarioDTO converterUnico(Usuario usuario) {
		return new UsuarioDTO(usuario);
	}
}
