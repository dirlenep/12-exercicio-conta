package com.example.projetoconta.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projetoconta.dto.ContaDTO;
import com.example.projetoconta.form.ContaForm;
import com.example.projetoconta.form.UpdateContaForm;
import com.example.projetoconta.model.Conta;
import com.example.projetoconta.repository.ContaRepository;

@RestController
@RequestMapping("/conta")
public class ContaController {
		
	@Autowired
	private ContaRepository contaRepository;
	
	//GET
	@GetMapping
	public List<ContaDTO> listar(){
		List<Conta> contas = contaRepository.findAll(); 
		return ContaDTO.converter(contas);
	}
	
	//GET COM ID
	@GetMapping("/{contaId}")
	public Conta buscarUser(@PathVariable Integer contaId) {
		return contaRepository.getById(contaId);
	}
	
	//POST 
	@PostMapping
	@Transactional
	public Conta salvar(@RequestBody @Valid ContaForm contaForm) {
		Conta conta = contaForm.converter();
		return conta;
	}
	
	//PUT COM ID 
	@PutMapping("/{contaId}")
	@Transactional
	public ResponseEntity<ContaDTO> atualizar(@PathVariable int contaId, @RequestBody UpdateContaForm contaForm){
		Conta conta = contaForm.atualizar(contaId, contaRepository);
		if (conta != null) {
			return ResponseEntity.ok(new ContaDTO(conta));
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	//DELETE COM ID 
	@DeleteMapping("/{contaId}")
	@Transactional
	public ResponseEntity<?> deletar(@PathVariable int contaId){
		Optional<Conta> contaOp = contaRepository.findById(contaId);
		if(contaOp.isPresent()) {
			contaRepository.deleteById(contaId);
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}