package com.example.projetoconta.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projetoconta.dto.EnderecoDTO;
import com.example.projetoconta.form.EnderecoForm;
import com.example.projetoconta.form.UpdateEnderecoForm;
import com.example.projetoconta.model.Endereco;
import com.example.projetoconta.repository.EnderecoRepository;

@RestController
@RequestMapping("/endereco")
public class EnderecoController {
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@GetMapping
	public List<EnderecoDTO> listar(String cep){
		List<Endereco> enderecos;
		if(cep == null) {
			enderecos = enderecoRepository.findAll(); 
		} else {
			enderecos = enderecoRepository.findByCep(cep);
		}
		return EnderecoDTO.converter(enderecos); 
	}
	
	@GetMapping("/{enderecoId}")
	public EnderecoDTO buscarEndereco(@PathVariable int enderecoId) {
		Optional<Endereco> endereco = enderecoRepository.findById(enderecoId);
		return EnderecoDTO.converterUnico(endereco.get());
	}
	
	@PostMapping
	@Transactional
	public Endereco salvar(@RequestBody EnderecoForm enderecoForm) {
		Endereco endereco = enderecoForm.converter();
		enderecoRepository.save(endereco);
		return endereco;
	}
	
	@PutMapping("/{enderecoId}")
	@Transactional
	public Endereco atualizar(@PathVariable int enderecoId, @RequestBody UpdateEnderecoForm enderecoForm) {
		Endereco endereco = enderecoForm.atualizar(enderecoId, enderecoRepository); 
		return endereco;
	}
	
	@DeleteMapping("/{enderecoId}")
	@Transactional
	public Endereco deletar(@PathVariable int enderecoId) {
		enderecoRepository.deleteById(enderecoId);
		return null;
	}
}
