package com.example.projetoconta.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.projetoconta.dto.UsuarioDTO;
import com.example.projetoconta.form.UpdateUsuarioForm;
import com.example.projetoconta.form.UsuarioForm;
import com.example.projetoconta.model.Usuario;
import com.example.projetoconta.repository.UsuarioRepository;

@RestController
@RequestMapping("/user")
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	// ROTA GET GERAL
	@GetMapping
	public List<UsuarioDTO> listar(String tipo){
		List<Usuario> usuarios;
		if (tipo == null) {
			usuarios = usuarioRepository.findAll();
		} else {
			usuarios = usuarioRepository.findByTipo(tipo);
			//usuarios = usuarioRepository.findByEndCep(cep);
		}
		return UsuarioDTO.converter(usuarios); 
	}
	
	// ROTA GET COM ID ESPECIFICO
	@GetMapping("/{userId}")
	public UsuarioDTO buscarUser(@PathVariable String userId) {
		Optional<Usuario> user = usuarioRepository.findById(userId);
		return UsuarioDTO.converterUnico(user.get());
	}
		
	// ROTA POST 
	@PostMapping
	public Usuario salvar(@RequestBody @Valid UsuarioForm userForm) {
		Usuario user = userForm.converter();
		usuarioRepository.save(user);
		return user;
	}
	
	// ROTA PUT - PRECISA ID ESPECIFICO 
	@PutMapping("/{userId}")
	public Usuario atualizar(@PathVariable String userId, @RequestBody UpdateUsuarioForm userForm) {
		Usuario user = userForm.atualizar(userId, usuarioRepository); 
		return user;
	}
	
	// ROTA DELETE - PRECISA ID ESPECIFICO 
	@DeleteMapping("/{userId}")
	public Usuario deletar(@PathVariable String userId) {
		usuarioRepository.deleteById(userId);
		return null;
	}
}