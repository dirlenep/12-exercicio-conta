package com.example.projetoconta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projetoconta.model.Conta;

public interface ContaRepository extends JpaRepository<Conta, Integer>{

}
