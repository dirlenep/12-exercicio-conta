package com.example.projetoconta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.projetoconta.model.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

	List<Endereco> findByCep(String cep);
	
}
