package com.example.projetoconta.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.projetoconta.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, String>  {
	
	List<Usuario> findByTipo(String tipo);

	List<Usuario> findByEndCep(String cep);
	
}
