package com.example.projetoconta.model;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Endereco {
	
	@Id @NotNull @NotEmpty @NotBlank
	private int enderecoId;
	private String rua;
	private int numero;
	private String bairro;
	private String cidade;
	private String estado;
	@NotNull @NotEmpty @NotBlank
	private String cep;
	
	public Endereco() {}
	
	public Endereco(int enderecoId, String rua, int numero, String bairro, String cidade, String estado, String cep) {
		this.enderecoId = enderecoId;
		this.rua = rua;
		this.numero = numero;
		this.bairro = bairro;
		this.cidade = cidade;
		this.estado = estado;
		this.cep = cep;
	}
	
	public int getEnderecoId() {
		return enderecoId;
	}
	public void setEnderecoId(int enderecoId) {
		this.enderecoId = enderecoId;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
}
