package com.example.projetoconta.model;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Usuario {	

	@Id @NotNull @NotEmpty @NotBlank
	private String userId;
	@NotNull @NotEmpty @NotBlank
	private String senha;
	@NotNull @NotEmpty @NotBlank
	private String nome;
	@NotNull @NotEmpty @NotBlank
	private String cpf;
	@NotNull @NotEmpty @NotBlank
	private String tipo;
	
	@ManyToOne @JoinColumn(name="end_endereco_id")
	private Endereco end;
	
	@OneToOne @JoinColumn(name="num_Conta_id")
	private Conta conta;
	
	public Usuario() {}
		
	public Usuario(String userId, String senha, String nome, String cpf, String tipo, Endereco end, Conta conta) {
		setUserId(userId);
		setSenha(senha);
		setNome(nome);
		setCpf(cpf);
		setTipo(tipo);
		setEnd(end);
		setConta(conta);
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Endereco getEnd() {
		return end;
	}
	public void setEnd(Endereco end) {
		this.end = end;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	
}
