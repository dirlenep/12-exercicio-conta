package com.example.projetoconta.model;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Conta {
	
	@Id @NotNull @NotEmpty @NotBlank
	private int numConta;
	@NotNull @NotEmpty @NotBlank
	private double saldo;
	@NotNull @NotEmpty @NotBlank
	private double limite;
	@NotNull @NotEmpty @NotBlank
	private int tipo;
	@NotNull @NotEmpty @NotBlank
	private int senha;
	
	public Conta() {}
	
	public Conta(int numConta, double saldo, double limite, int tipo, int senha) {
		setNumConta(numConta);
		setSaldo(saldo);
		setLimite(limite);
		setTipo(tipo);
		setSenha(senha);
	}
	
	public int getNumConta() {
		return numConta;
	}
	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLimite() {
		return limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getSenha() {
		return senha;
	}
	public void setSenha(int senha) {
		this.senha = senha;
	}
	
	public void depositar(double valorDeposito) {
		if (valorDeposito < 0) {
			System.out.println("Valor do depósito precisa ser acima de 0");
		} 
		this.saldo = this.saldo + valorDeposito;
	}
	
	public void sacar(double valorSaque) {
		if ((this.saldo + this.limite) < valorSaque) {
			System.out.println("Saldo Insuficiente");
		}
		this.saldo = this.saldo - valorSaque;
	}
}
