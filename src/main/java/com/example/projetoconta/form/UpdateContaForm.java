package com.example.projetoconta.form;

import java.util.Optional;

import com.example.projetoconta.model.Conta;
import com.example.projetoconta.repository.ContaRepository;

public class UpdateContaForm {
	private int senha;

	public int getSenha() {
		return senha;
	}
	public void setSenha(int senha) {
		this.senha = senha;
	}
	
	public Conta atualizar(int contaId, ContaRepository contaRepository) {
		Optional<Conta> conta = contaRepository.findById(contaId);
		if (conta.isPresent()) {
			conta.get().setSenha(this.senha);
			return conta.get();
		} else {
			return null;
		}
	}
}
