package com.example.projetoconta.form;

import java.util.Optional;

import com.example.projetoconta.model.Usuario;
import com.example.projetoconta.repository.UsuarioRepository;

public class UpdateUsuarioForm {
	private String senha;

	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
		
	public Usuario atualizar(String userId, UsuarioRepository usuarioRepository) {
		Optional<Usuario> user = usuarioRepository.findById(userId);
		user.get().setSenha(this.senha);
		return user.get();
	}
}