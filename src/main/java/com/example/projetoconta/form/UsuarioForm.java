package com.example.projetoconta.form;

import com.example.projetoconta.model.Conta;
import com.example.projetoconta.model.Endereco;
import com.example.projetoconta.model.Usuario;

public class UsuarioForm {
	private String userId;
	private String senha;
	private String nome;
	private String cpf;
	private String tipo;
	private Endereco end;
	private Conta conta;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Endereco getEnd() {
		return end;
	}
	public void setEnd(Endereco end) {
		this.end = end;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public Usuario converter() {
		return new Usuario(userId, senha, nome, cpf, tipo, end, conta);
	}
}
