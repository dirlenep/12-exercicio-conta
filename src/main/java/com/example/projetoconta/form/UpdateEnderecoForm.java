package com.example.projetoconta.form;

import java.util.Optional;

import com.example.projetoconta.model.Endereco;
import com.example.projetoconta.repository.EnderecoRepository;

public class UpdateEnderecoForm {
	private String rua;
	private int numero;
	private String bairro;
	private String cidade;
	private String estado;
	private String cep;

	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}

	public Endereco atualizar(int enderecoId, EnderecoRepository enderecoRepository) {		
		Optional<Endereco> endereco = enderecoRepository.findById(enderecoId);
		endereco.get().setRua(this.rua);
		endereco.get().setNumero(this.numero);
		endereco.get().setBairro(this.bairro);
		endereco.get().setCidade(this.cidade);
		endereco.get().setEstado(this.estado);
		endereco.get().setCep(this.cep);
		
		return endereco.get();
	}

}
